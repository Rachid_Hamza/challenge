/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojbootstrap', 'ojs/ojinputtext',
    'ojs/ojarraydataprovider', 'ojs/ojlabel', 'ojs/ojknockout', 'ojs/ojchart',
    'ojs/ojbutton', 'ojs/ojchart', 'ojs/ojtoolbar','ojs/ojselectcombobox'
  ],

function (oj, ko, $, Bootstrap, data, ArrayDataProvider) {

    function DashboardViewModel() {
      var self = this;


      var val = $('#datachart').val;
      console.log(val);


      self.datax = "Serie 1\\tGroupe 1\\t15\\t25\\t5\\nSerie 2\\tGroupe 2\\t10\\t20\\t5\\nSerie 1\\tGroupe 1\\t20\\t25\\t8";
      var ourFormat = "Serie 1\tGroupe 1\t15\t25\t5\nSerie 2\tGroupe 2\t10\t20\t5\nSerie 1\tGroupe 1\t20\t25\t8";
      var ourFormat2 = "Serie 1\tGroupe A\t26\t71\t74\t73\t48\t52\nSerie 2\tGroupe B\t1\t58\t36\t14\t37\t50\nSerie 1\tGroupe A\t3\t28\t59\t8\t12\t17";
 

      self.shouldShowChart = ko.observable(true);
      self.shouldShowChart2 = ko.observable(true);


      this.val = ko.observable("Chrome");

      var data1 = [];
      var lines = ourFormat.split('\n');
      lines.forEach((element, index) => {
        valeurs = element.split('\t');
        var obj = {
          id: index,
          series: valeurs[0],
          group: valeurs[1],
          x: parseInt(valeurs[2]),
          y: parseInt(valeurs[3]),
          z: parseInt(valeurs[4])
        };
        data1.push(obj);

      });

      var data2 = [];
      var lines = ourFormat2.split('\n');
      lines.forEach((element, index) => {
        valeurs = element.split('\t');
        var obj = {
          id: index,
          series: valeurs[0],
          group: valeurs[1],
          low: parseInt(valeurs[2]),
          high: parseInt(valeurs[3]),
          z: parseInt(valeurs[4]),
          q1: parseInt(valeurs[5]),
          q2: parseInt(valeurs[6]),
          q3: parseInt(valeurs[7]),
          outliers: [9, 12, 78]
        };
        data2.push(obj);

      });


      // self.datasource=ko.observableArray(text);
      this.dataProvider = new ArrayDataProvider(JSON.parse(JSON.stringify(data1)), {
        keyAttributes: 'id'
      });


      this.dataProvider2 = new ArrayDataProvider(JSON.parse(JSON.stringify(data2)), {
        keyAttributes: 'id'
      });
      /* toggle buttons */
      this.orientationValue = ko.observable('vertical');
      // 
      this.getScatterChart = function () {
        console.log("Scatter Chart ");
        this.shouldShowChart(!this.shouldShowChart());




      };
      // 
      getBoxPlotDiagram = function () {
        console.log("BoxPlot Diagram ");
        this.shouldShowChart2(!this.shouldShowChart2());
      };

      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new DashboardViewModel();
  }
);
